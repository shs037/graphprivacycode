'''
This is the graph projection algorithm from paper:
Day, Wei-Yen, Ninghui Li, and Min Lyu. "Publishing graph degree distribution with node differential privacy." Proceedings of the 2016 International Conference on Management of Data. ACM, 2016.
The algorithm is adapted to graph sequence.

Input:
  ordered_edges: edges ordered by time
  Gs: the graph sequence
  edgeOpt: edge type (directed or undirected)
  projectParam: projection parameter. projectParam[0] for in-degree (or degree for undirected graph), projectParam[1] for out-degree.
Output:
  degrees: a list, where degrees[i] represents the degree sequence at time i
  numedges: a list, where numedges[i] represents # of ordered_edges at time i
'''

import networkx as nx
import copy

def DLL_simplified(ordered_edges, Gs, edgeOpt, projectParam):
  Y = len(Gs)
  n = Gs[-1].number_of_nodes()
  degrees = [0] * Y

  if projectParam[0] == 0 or projectParam[1] == 0:
    for i in range(Y):
      degrees[i] = [0] * n
    numedges = [0] * Y
    return degrees, numedges

  num_edges_year = [0] * Y
  for i in range(Y):
    num_edges_year[i] = Gs[i].number_of_edges()

  degreesO = [0] * n # the in-degree of every node
  degreesI = [0] * n # the out-degree (or degree for undirected graph) of every node
  numedges = [0] * Y # total # of ordered_edges every year
  numedge = 0
  j = 0
  for i in range(len(ordered_edges)): # look at ordered_edges one by one
    u, v = ordered_edges[i] # edge from u to v for directed graph

    # check if degrees of u and v have reached projectParam, if not, add the edge
    if edgeOpt == 'undirected':
      if degreesO[u] < projectParam[0] and degreesO[v] < projectParam[0]:
        degreesO[u] += 1
        degreesO[v] += 1
        numedge += 1
    else:
      if degreesO[u] < projectParam[1] and degreesI[v] < projectParam[0]:
        degreesO[u] += 1
        degreesI[v] += 1
        numedge += 1

    while(j < Y and i + 1 == num_edges_year[j]): # need to output
      degrees[j] = [_ for _ in degreesO]
      numedges[j] = numedge
      j += 1

  return degrees, numedges
