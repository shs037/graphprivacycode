'''
A helper for function BAGraph_evolvingGraphs. Generate an online graph w/ Barabasi-Albert model based on an existing graph

Input:
  ordered_edges: edges ordered by time
  edi: # of nodes a new node connects with
  p: probability for a new node to be an isolated node
  m: # of nodes a new node connects with
  num_years: # of years
  num_nodes_per_year: # of new nodes/patients added every year
  edgeOpt: whether to build direct or undirect graph
  decayOpt: option for decay w/ time. 0 for not decay, 1 for exponential, 2 for power
  decay: the corresponding decay parameter

Output:
  ordered_edges: edges ordered by time (after the update made by this function)
  edi: edi of nodes (after the update made by this function)
'''

from getDegreeFromEdges import *
from sample_m import *
import random

def BAGraph_grow(ordered_edges, degrees, edi, p, m, num_years, num_nodes_per_year, edgeOpt, decayOpt, decay):
  # check values
  if decayOpt not in [0, 1, 2]:
    raise ValueError('Invalid value for decayOpt.')

  if decayOpt != 0 and decay < 0:
      raise ValueError('Invalid value for decay.')

  if len(edi) < m:
    raise ValueError('# of existing nodes should be >= m.')

  num_initial = len(edi) # number of existing nodes
  year_initial = max(edi) # years already passed

  degrees += [0] * (num_years * num_nodes_per_year)
  edi += [0] * (num_years * num_nodes_per_year)

  for i in range(num_years):
    cur_year = year_initial + i + 1
    for j in range(num_nodes_per_year):
      idx = num_initial + i * num_nodes_per_year + j # idx of new node
      edi[idx] = cur_year
      if random.random() < p: # if rand < p, make it an isolated node
        continue

      # calculate connection probability
      prob = [d + 1.0 for d in degrees[:idx]]
      if decayOpt == 1:
        prob = [pr * math.exp((e - cur_year) * decay) for pr, e in zip(prob, edi[:idx])]
      elif decayOpt == 2:
        prob = [pr * (cur_year - e + 1) ** (-decay) for pr, e in zip(prob, edi[:idx])]
      # sample m nodes to connect with
      connect = sample_m(prob, m)
      ordered_edges += [[c, idx] for c in connect]
      if max(connect) >= idx:
        raise ValueError('err')

      if edgeOpt == 'undirected':
        degrees[idx] += m
      for c in connect:
        degrees[c] += 1

  return ordered_edges, degrees, edi
