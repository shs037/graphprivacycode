'''
Sample m elts from a given distribution.

Input:
  prob: probability distribution
  m: # of elements to sample from 0 to len(prob) - 1
Output:
  The sampled elements
'''
import numpy as np

def sample_m(prob, m):
  if m > len(prob):
    print("sample_m gets invalid input (m larger than # of elt)")
    return []
  if m == len(prob):
    return list(range(len(prob)))

  sum_prob = sum(prob)
  if sum_prob != 1:
    prob = [float(p) / sum_prob for p in prob]

  selected = set()
  n_elts = len(prob) # total # of elts
  while(len(selected) < m):
    new_selected = np.random.choice(n_elts, 2*m, p=prob)
    selected = selected.union(set(new_selected))

  return list(selected)[:m]
