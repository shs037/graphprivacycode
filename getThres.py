'''
Calculating the threshold for high-degree-node, i.e., 90-th percentile of the actual degree distribution in the underlying graph.
'''

import math
import numpy as np

def getThres(publish, degType, Gs):
  if publish == 'highDeg':
    if degType == 'degree':
      thres = np.percentile(zip(*Gs[-1].degree())[1], 90, interpolation='higher')
    elif degType == 'outdegree':
      thres = np.percentile(zip(*Gs[-1].out_degree())[1], 90, interpolation='higher')
    thres = max(thres, 1)
  else:
    thres = -1

  return thres
