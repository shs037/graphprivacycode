'''
Generate an online graph w/ Barabasi-Albert model

Input:
  p: probability for a new node to be an isolated node
  m: # of nodes a new node connects with
  num_years: # of years
  num_nodes_per_year: # of new nodes/patients added every year
  num_initial: # of initial nodes in the BA model
  output_years: when we need to release the statisticas of a graph
  edgeOpt: whether to build direct or undirect graph
  decayOpt: option for decaying w/ time. 0 for not decay, 1 for exp, 2 for power
  decay: the corresponding parameter for decaying

Output:
  Gs: a list of graphs, where Gs[i] represents the graph in output_years[i]
  edis: a list of edi, where edis[i] represents edis of nodes in output_years[i]
  ordered_edges: edges ordered by time
'''

from BAGraph_grow import *
import networkx as nx

def BAGraph_evolvingGraphs(p, m, num_years, num_nodes_per_year, num_initial, output_years, edgeOpt, decayOpt, decay):

  if max(output_years) > num_years:
    raise ValueError('output_years should <= num_years.')

  if output_years[-1] != num_years:
    output_years.append(num_years)

  edis = [] # record edi of each year
  Gs = [] # record graph of each year

  # initial graph - several isolated nodes
  edi = [0] * num_initial # year starts from 0 as initial
  degrees = [0] * num_initial

  output_years = [0] + output_years
  ordered_edges = [] # record edges in order
  for i in range(len(output_years) - 1):
    cur_num_years = output_years[i + 1] - output_years[i]
    ordered_edges, degrees, edi = BAGraph_grow(ordered_edges, degrees, edi, p, m, cur_num_years, num_nodes_per_year, edgeOpt, decayOpt, decay)

    if edgeOpt == 'undirected':
      Gs.append(nx.Graph())
    else:
      Gs.append(nx.DiGraph())
    Gs[-1].add_nodes_from(range(len(edi)))
    Gs[-1].add_edges_from(ordered_edges)
    edis.append([_ for _ in edi])

  return Gs, edis, ordered_edges
