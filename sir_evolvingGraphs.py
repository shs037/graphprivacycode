'''
Generate an online graph w/ SIR model

Input:
  G: the underlying connection (nodes are assumed to ranges from 0, 1, 2, ...)
  perct_initial: percentage of nodes that are infected in the beginning
  rate_transmit: transmission rate
  rate_recover: recover rate
  num_years: # of years
  output_years: when we need to release the statisticas of a graph
  edgeOpt: if edges are directed or undirected

Output:
  Gs: a list of graphs, where Gs[i] represents the graph in output_years[i]
  edis: a list of edi, where edis[i] represents edis of nodes in output_years[i]
  ordered_edges: edges ordered by time
'''

import random
import networkx as nx

def sir_evolvingGraphs(G, perct_initial, rate_transmit, rate_recover, num_years, output_years, edgeOpt):

  if isinstance(rate_recover, float): # rate_recover can be different for every year
    rate_recover = [rate_recover] * (max(output_years) + 1)

  nodes = list(range(G.number_of_nodes()))
  n = max(nodes)
  mapping = {} # mapping[i] is the new idx of node i in G

  # infected = set() # all nodes initialize as not infected
  immuned = set() # all nodes initialize as not immuned

  # initial graph - several infected nodes
  num_initial = int(round(perct_initial * len(nodes)))
  infected = set(random.sample(nodes, num_initial))
  mapping = {e:i for i,e in enumerate(infected)}
  curIdx = len(infected) - 1 # index 0 to curIdx used

  edi = [0] * num_initial # year starts from 0 as initial

  # edis and Gs record the edi and graph of every year
  edis = []
  Gs = []

  ordered_edges = []
  for i in range(1, max(output_years) + 1): # start evolving
    # some recover
    getImmuned = set([im for im in infected if random.random() < rate_recover[i]]) # currently infected & prob
    immuned.update(getImmuned)
    infected.difference_update(getImmuned)

    # some transmit. transmission goes through every edge w/ some prob
    infected_old = infected.copy()
    for node in infected_old:
      ngb = list(G.neighbors(node))
      if len(ngb) == 0:
        continue
      getInfected = [infe for infe in ngb if infe not in immuned and infe not in infected and random.random() < rate_transmit / float(len(ngb))]
      infected.update(getInfected)
      edi += [i] * len(getInfected)
      for infe in getInfected:
        mapping[infe] = curIdx + 1
        ordered_edges.append([mapping[node], curIdx + 1])
        curIdx += 1

    # output graph when needed
    if i in output_years:
      if edgeOpt == 'undirected':
         Gs.append(nx.Graph())
      else:
         Gs.append(nx.DiGraph())
      Gs[-1].add_nodes_from(range(curIdx + 1))
      Gs[-1].add_edges_from(ordered_edges)
      edis.append([_ for _ in edi])

  return Gs, edis, ordered_edges
