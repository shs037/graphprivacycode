'''
A function that calculates the difference between consecutive elements in a list.
'''
def diff(a):
  return [x - a[i - 1] for i, x in enumerate(a) if i > 0]
