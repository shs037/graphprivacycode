'''
Run the Laplace mechanism and calculate averaged error.

Input:
    noise_scale: scale of the Laplace noise
    estimated_val: estimated value of the statistic on which noise is added
    true_ans: true value of the statistic
    opt: value 'cumsum' indicates that the actual statistic is a cummulative sum of the input values (used for senseDiff)
    rep: # of repeated runs of the Laplace mechanism
Output:
    err: the averaged error over repetition
'''

import numpy as np

def laplaceMech(noise_scale, estimated_val, true_ans, opt, rep=100):

  if opt == 'cumsum':
    true_ans = np.cumsum(true_ans)

  err = 0
  for _ in range(rep):
    noise = np.random.laplace(0, noise_scale, len(estimated_val))
    h_noisy = [estimated_val[i] + noise[i] for i in range(len(estimated_val))]

    if opt == 'cumsum':
      noisy_ans = np.cumsum(h_noisy)
    else:
      noisy_ans = h_noisy

    err += sum([abs(true_ans[i] - noisy_ans[i]) / true_ans[i] for i in range(len(estimated_val))])

  err /= float(rep)

  return err
