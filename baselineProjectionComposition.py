'''
The compose-projection algorithm.
Input:
  ordered_edges: edges ordered by time
  Gs: the graph sequence
  edgeOpt: whether to build direct or undirect graph
  degType: type of degree
  publish: type of statistics to publish, either highDeg (for # of high-degree-nodes) or edge (for # of edges)
  thres: threshold for high-degree node
  epsilons: a list of epsilon value to use
  projectParams: parameters to try for the projection step. Try 10 equally-spaced value in the whole range when given an empty list.
Output:
  errs: the averaged error over repetition for each epsilon value
'''

import networkx as nx
import numpy as np
from gethighDegNodes import *
from getedges import *
from DLL_simplified import *
from laplaceMech import *

def baselineProjectionComposition(ordered_edges, Gs, edgeOpt, degType, publish, thres, epsilons, projectParams=[]):

  m = len(Gs)
  if publish == 'highDeg':
    true_value = gethighDegNodes(Gs, thres, degType);
  else:
    true_value = getedges(Gs)

  if degType == 'degree':
    maxDeg = max(zip(*Gs[-1].degree())[1])
    isDirected = False
  elif degType == 'outdegree':
    if publish == 'highDeg':
      maxDeg = max(zip(*Gs[-1].out_degree())[1])
    else:
      maxDeg = max(max(zip(*Gs[-1].out_degree())[1]), max(zip(*Gs[-1].in_degree())[1]))
    isDirected = True


  if len(projectParams) == 0:
    if publish == 'highDeg':
      projectParams = list(range(thres - 1, maxDeg + 1))
      if isDirected:
        projectParams = [x for x in projectParams if x >= 2] # if its < 2, Din = it-2<0 and the proj doesn't make sense
    else:
      projectParams = list(range(1, maxDeg + 1))

    if len(projectParams) > 10: # search from ~10 values
      step = int(round((projectParams[-1] - projectParams[0]) / 10.0))
      projectParams = list(range(projectParams[0], projectParams[-1] + 1, step))

  projectParams = [float(i) for i in projectParams]

  bias = [0] * len(projectParams)
  value_proj = [] # statistics after projection
  for j in range(len(projectParams)):
    if publish == 'highDeg':
      if not isDirected:
        projectParam = [projectParams[j]] * 2
      else:
        projectParam = [projectParams[j] - 2, projectParams[j]]
    else:
      projectParam = [projectParams[j]] * 2

    degrees_new, numedges_new = DLL_simplified(ordered_edges, Gs, edgeOpt, projectParam)

    if publish == 'highDeg':
      value_proj.append(gethighDegNodes(degrees_new, thres, degType))
    else:
      value_proj.append([_ for _ in numedges_new])

    bias[j] = sum([abs(true_value[_] - value_proj[j][_]) for _ in range(len(value_proj[j]))])

  if publish == 'highDeg':
    if isDirected:
      sensitivity = [x - 1 for x in projectParams]
    else:
      sensitivity = [x + 1 for x in projectParams]
  else:
    if isDirected:
      sensitivity = [x * 2 for x in projectParams]
    else:
      sensitivity = [x for x in projectParams]

  errs = [0] * len(epsilons)
  for i in range(len(epsilons)):
    j = np.argmin([bias[_] + sensitivity[_] / (epsilons[i] / m) for _ in range(len(projectParams))])
    errs[i] = laplaceMech(sensitivity[j] / (epsilons[i] / m), value_proj[j], true_value, '')

  return errs
