'''
Get the degrees of nodes from a list of edges
Input:
  edges: a list of edges
Output:
  degree: a list of degrees of nodes
'''

def getDegreeFromEdges(edges):
  degree = {}
  for u, v in edges:
    degree[u] = degree.get(u, 0) + 1
    degree[v] = degree.get(v, 0) + 1
  degree = [degree[u] for u in range(len(degree))]
  return degree
