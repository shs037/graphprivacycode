'''
Calculate the sensitivity for senseDiff and compose-D-bounded.

Input:
    publish: type of statistics to publish, either highDeg (for # of high-degree-nodes) or edge (for # of edges)
    degType: type of degree (currently implemented for degree (of undirected graph) and out-degree only (for directed graph))
    Gs: the graph sequence
    roundUp: whether estimate the actual max-degree up to the nearest 5

Output:
    sensitivity: sensitivity used in senseDiff
'''


import math
import numpy as np
import networkx as nx

def getSensitivity(publish, degType, Gs, roundUp=True):
  if publish == 'highDeg':
    if degType == 'degree':
      maxDegtesting = max(zip(*Gs[-1].degree())[1])
    elif degType == 'outdegree':
      maxDegtesting = max(zip(*Gs[-1].in_degree())[1])

    if roundUp:
      sensitivity = 2.0 * math.ceil(maxDegtesting * 0.2) / 0.2 + 1
    else:
      sensitivity = 2.0 * maxDegtesting + 1

  else:
    if degType == 'degree':
      if roundUp:
        sensitivity = math.ceil(max(zip(*Gs[-1].degree())[1]) * 0.2) / 0.2
      else:
        sensitivity = max(zip(*Gs[-1].degree())[1])
    else:
      if roundUp:
        sensitivity = math.ceil(max(zip(*Gs[-1].in_degree())[1]) * 0.2) / 0.2 + math.ceil(max(zip(*Gs[-1].out_degree())[1]) * 0.2) / 0.2
      else:
        sensitivity = max(zip(*Gs[-1].in_degree())[1]) + max(zip(*Gs[-1].out_degree())[1])

  return float(sensitivity)
