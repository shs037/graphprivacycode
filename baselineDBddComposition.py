'''
The compose-D-bounded algorithm.
Input:
  Gs: the graph sequence
  degType: type of degree
  publish: type of statistics to publish, either highDeg (for # of high-degree-nodes) or edge (for # of edges)
  thres: threshold for high-degree node
  epsilons: a list of epsilon value to use
Output:
  errs: the averaged error over repetition for each epsilon value
'''

from getSensitivity import *
from gethighDegNodes import *
from getedges import *
from laplaceMech import *


def baselineDBddComposition(Gs, degType, publish, thres, epsilons):

  sensitivity = getSensitivity(publish, degType, Gs) # get sensitivity for senseDiff and compose-D-bounded

  # get the actual (non-private) value
  if publish == 'highDeg':
    stats_orig = gethighDegNodes(Gs, thres, degType)
  else:
    stats_orig = getedges(Gs)

  errs = [0] * len(epsilons) # record error
  for i in range(len(epsilons)):
    errs[i] = laplaceMech(sensitivity / (epsilons[i]/float(len(Gs))), stats_orig, stats_orig, '')

  return errs
