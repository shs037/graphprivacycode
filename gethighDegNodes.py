'''
Get # of high-degree-nodes for every graph in a graph sequence.
Input can either be a sequence of graph or a sequence of degrees.
'''

def gethighDegNodes(inputs, thres, opt='degree'):

  res = [0] * len(inputs)

  if isinstance(inputs[0], list):
    for i in range(len(inputs)):
      res[i] = sum([_ >= thres for _ in inputs[i]])

  else:
    for i in range(len(inputs)):
      if opt == 'degree':
        res[i] = sum([_ >= thres for _ in zip(*inputs[i].degree())[1]])
      elif opt == 'indegree':
        res[i] = sum([_ >= thres for _ in zip(*inputs[i].in_degree())[1]])
      elif opt == 'outdegree':
        res[i] = sum([_ >= thres for _ in zip(*inputs[i].out_degree())[1]])
      else:
        print('opt gets wrong value.')

  return res
