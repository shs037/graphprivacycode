'''
This is an example for how to use the functions.
It runs the three algorithms (senseDiff, compose-projection, compose-D-bounded) on a given graph sequence under a set of given epsilons, and plot the results.
Built on python 2.7 and networkx 2.1
'''

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

from generate_graphs import *
from getSensitivity import *
from getThres import *
from gethighDegNodes import *
from getedges import *
from laplaceMech import *
from baselineProjectionComposition import *
from baselineDBddComposition import *
from senseDiff import *

## parameters
graphName = 'synth_SIR'
edgeOpt = 'undirected'
degType = 'outdegree'
publish = 'highDeg'
roundUp = True
yearStep = 1
if edgeOpt == 'undirected':
  degType = 'degree'

epsilons = range(1, 11)
epsilons = [float(i) for i in epsilons]

## read data
print('Read / generate data')
Gs, edis, ordered_edges = generate_graphs(graphName, edgeOpt, yearStep)
years = list(range(len(Gs)))
thres = getThres(publish, degType, Gs) # get threshold for high-degree-node (set to -1 for # of edges)

## Run three algorithms and calculate the error
print('Run algorithms')
errs = senseDiff(Gs, degType, publish, thres, epsilons) # error for senseDiff
errs_baseline_proj = baselineProjectionComposition(ordered_edges, Gs, edgeOpt, degType, publish, thres, epsilons) # error for compose-projection
errs_baseline_DBdd = baselineDBddComposition(Gs, degType, publish, thres, epsilons) # error for compose-D-bounded

## Plot
print('Plot')
plt.plot(epsilons, errs, 'C0', label='senseDiff')
plt.plot(epsilons, errs_baseline_proj, 'C1', label='compose-projection')
plt.plot(epsilons, errs_baseline_DBdd, 'C2', label='compose-D-bounded')
plt.xlabel('epsilon')
plt.ylabel('normalized err')
plt.grid(True)
plt.legend()
plt.show()
