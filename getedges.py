'''
Get # of edges for every graph in a graph sequence.
'''

def getedges(Gs):
  res = []
  for i in range(len(Gs)):
    res.append(Gs[i].number_of_edges())
  return res
