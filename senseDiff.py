'''
The senseDiff algorithm.
Input:
  Gs: the graph sequence
  degType: type of degree
  publish: type of statistics to publish, either highDeg (for # of high-degree-nodes) or edge (for # of edges)
  thres: threshold for high-degree node
  epsilons: a list of epsilon value to use
Output:
  errs: the averaged error over repetition for each epsilon value
'''

from getSensitivity import *
from gethighDegNodes import *
from getedges import *
from laplaceMech import *
from utility import *


def senseDiff(Gs, degType, publish, thres, epsilons):

  sensitivity = getSensitivity(publish, degType, Gs) # get sensitivity for senseDiff and compose-D-bounded

  # get the actual (non-private) value
  if publish == 'highDeg':
    stats_orig = gethighDegNodes(Gs, thres, degType)
  else:
    stats_orig = getedges(Gs)
  stats_diff = diff([0] + stats_orig) # calculate the difference sequence

  errs = [0] * len(epsilons) # record error
  for i in range(len(epsilons)):
    errs[i] = laplaceMech(sensitivity / epsilons[i], stats_diff, stats_diff, 'cumsum')

  return errs
